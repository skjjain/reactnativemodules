import React, {Component} from 'react';
import {Platform, Text, View, Animated} from 'react-native';
import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';
import Deck from './src/deck';

import {Button, Card} from 'react-native-elements';


const DATA = [
  { id: 1, text: 'Card #1', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 2, text: 'Card #2', uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 3, text: 'Card #3', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 4, text: 'Card #4', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
  { id: 5, text: 'Card #5', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 6, text: 'Card #6', uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 7, text: 'Card #7', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 8, text: 'Card #8', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
];

type Props = {};
class LandingPage extends Component<Props> {

  renderCard = (item) => {
    return (
    <Card key={item.id}
          title={item.text}
          image={{uri:item.uri}}
    >
    <Text>I can customize the card</Text>
    <Button icon={{name:'code'}}
    backgroundColor="#03A9F4"
    title="View Now!"
    ></Button>
    </Card>);
  }

  render() {
    return (
      <View >
        <Deck
        data={DATA}
        renderCard={this.renderCard}
        />
      </View>
    );
  }
}


class SpinAnimation extends Component<Props> {

  componentWillMount(){
    this.position = new Animated.ValueXY(0,0);
    Animated.spring(this.position, {
      toValue: {x:0, y:200}
    }).start();
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <Button
          onPress={() => navigation.toggleDrawer()}
          title="Menu"
        />
      ),
    };
  };
  render() {
    return (
      <Animated.View style={this.position.getLayout()}>
        <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
          <Text>Welcome to React Native!</Text>
        </View>
      </Animated.View>
    );
  }
}

const defNav = ({navigation}) => {
  return {
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerRight: (
      <Button
        onPress={() => navigation.toggleDrawer()}
        title="Menu"
      />
    ),
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
};

const HomeStack = createStackNavigator(
  {
    Home: LandingPage
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: defNav
  }
);


const SpinAnimationStack = createStackNavigator(
  {
    Spin: SpinAnimation
  },
  {
    initialRouteName: 'Spin',
    defaultNavigationOptions: defNav
  }
);
const DrawerNavigator = createDrawerNavigator(
  {
    Home: HomeStack,
    Spin: SpinAnimationStack
  }
);
export default createAppContainer(DrawerNavigator);
