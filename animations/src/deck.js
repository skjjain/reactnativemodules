import React, {Component} from 'react';
import {View, Animated, PanResponder, Dimensions} from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;

const SWIPE_THRESHOLD = SCREEN_WIDTH * 0.25;

const EXIT_DURATION = 350;

class Deck extends Component{

    static defaultProps = {
        onSwipeLeft : {}, 
        onSwipeRight : {}
    };

    constructor(props){
        super(props);

        const position = new Animated.ValueXY();

        const pRes =PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gesture) => {
                position.setValue({x: gesture.dx});
            },
            onPanResponderRelease: (event, gesture) => {
                if(gesture.dx > SWIPE_THRESHOLD){
                    // alert('Liked');
                    this.forceSwipe('right');
                }else if(gesture.dx < -SWIPE_THRESHOLD){
                    this.forceSwipe('left');
                }else{
                    this.resetPosition();
                }

            }
        });
        this.state = {pRes, position, index : 0};
        
    }

    forceSwipe(direction){
        Animated.timing(this.state.position, {
            toValue: {x: (direction == 'right' ? 1 : -1)*SCREEN_WIDTH  , y: 0},
            duration:EXIT_DURATION
        }).start(() => this.onSweepComplete(direction));
    }

    
    onSweepComplete(direction){
        // const {onSwipeLeft, onSwipeRight, data} = this.props;
        //this.state.position = new Animated.ValueXY(100, 100);
        this.setState({index: this.state.index + 1}); 
        Animated.spring(this.state.position, {
            toValue: {x: 0, y: 0}
        }).start();
    }

    resetPosition(){
        
        Animated.spring(this.state.position, {
            toValue: {x: 0, y: 0}
        }).start();
    }

    getCardStyle(){
        const {position} = this.state;

        const rotate = position.x.interpolate({
            inputRange:[-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5] ,
            outputRange:['-120deg', '0deg', '120deg']
        });
        return {
            ...position.getLayout(),
            transform: [{rotate}]
        }
    }

    renderCards(){
        return this.props.data.map( (item, index) => {
            
            if(index < this.state.index || index > this.state.index){
                return null;
            }

            // if(index == this.state.index){
            return (
                <Animated.View key={item.id} {...this.state.pRes.panHandlers} style={this.getCardStyle()}>
                    {this.props.renderCard(item)}
                </Animated.View>
            ); 
            // }

            // return this.props.renderCard(item)
        });
    }

    render(){
        return (
            <View>
                {this.renderCards()}
            </View>
        );
    }
}


export default Deck;